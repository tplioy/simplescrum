# == Schema Information
#
# Table name: product_backlog_items
#
#  id                 :integer         not null, primary key
#  product_backlog_id :integer
#  name               :string(255)
#  estimate           :integer
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#

class ProductBacklogItem < ActiveRecord::Base
  belongs_to :project

  validates :project_id, presence: true
end
