require 'ostruct'

class Presenter < OpenStruct
  def to_json(options = {})
    self.table.to_json
  end

  def as_json(options = {})
    self.table
  end
end
