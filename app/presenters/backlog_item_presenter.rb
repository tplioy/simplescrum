class BacklogItemPresenter < Presenter
  def initialize(item)
    super(
      :id => item.id,
      :name => item.name,
      :estimate => item.estimate
    ) unless item.nil?
  end
end
