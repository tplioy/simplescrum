class ProjectsPresenter < Presenter
  def initialize(projects)
    super(
      :projects => apply_presenter(projects)
    ) unless projects.nil?
  end

  private
  def apply_presenter(projects)
    projects.collect {|p| ProjectPresenter.new(p) }
  end
end
