class ProjectPresenter < Presenter
  def initialize(project)
    super(
      :id => project.id,
      :description => project.description,
      :name => project.name
    ) unless project.nil?
  end
end
