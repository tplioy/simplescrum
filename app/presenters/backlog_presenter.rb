class BacklogPresenter < Presenter
  def initialize(items)
    super(
      :items => apply_presenter(items)
    ) unless items.nil?
  end

  private
  def apply_presenter(items)
    items.collect {|i| BacklogItemPresenter.new(i) }
  end
end
