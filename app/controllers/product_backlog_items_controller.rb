class ProductBacklogItemsController < ApplicationController

  def index
    @project = Project.find_by_id(params[:id])
    if @project.nil?
      render :json => "not found", :status => 404
    else
      render :json => BacklogPresenter.new(@project.backlog_items), :status => 200
    end
  end

  def create
    @project = Project.find_by_id(params[:id])
    if @project.nil?
      render :json => "not found", :status => 404
    else
      @backlog_item = ProductBacklogItem.new(params[:item])
      @project.backlog_items << @backlog_item
      if @project.save
        render :json => @backlog_item, :status => 200
      else
        render :json => "error", :status => 500
      end
    end
  end

  def destroy
    @backlog_item = ProductBacklogItem.find_by_id_and_project_id(params[:id], params[:project_id])
    if @backlog_item.nil?
      render :json => "not found", :status => 404
    else
      if @backlog_item.destroy
        render :json => @backlog_item, :status => 200
      else
        render :json => "error", :status => 500
      end
    end
  end



end
