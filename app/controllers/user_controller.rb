class UserController < ApplicationController

  def index
    @user = User.all
    render :json => @user, :status => 200
  end

  def create
    @user = User.new(params[:user])
    if @user.save!
      render :json => @user, :status => 200
    end
  end

  def show
    @user = User.find_by_id(params[:id])
    if @user.nil?
      render :json => "erro", :status => 404
    else
      render :json => @user, :status => 200
    end
  end

  def destroy
    @user = User.find_by_id(params[:id])
    if @user.nil?
      render :json => "erro", :status => 404
    else
      if @user.destroy
        render :json => @user, :status => 200
      else
        render :json => "erro", :status => 500
      end
    end
  end

end
