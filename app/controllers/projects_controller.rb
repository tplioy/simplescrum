class ProjectsController < ApplicationController

  def index
    @projects = Project.all
    render :json => ProjectsPresenter.new(@projects), :status => 200
  end


  def create
    @project = Project.new(params[:project])
    if @project.save
      render :json => @project, :status => 200
    else
      render :json => "erro", :status => 500
    end
  end

  def destroy
    @project = Project.find_by_id(params[:id])
    if @project.nil?
      render :json => "erro", :status => 404
    else
      @project.destroy
      render :json => "", :status => 200
    end
  end

  def show
    @project = Project.find_by_id(params[:id])
    if @project.nil?
      render :json => "", :status => 404
    else
      render :json => @project, :status => 200
    end
  end

end
