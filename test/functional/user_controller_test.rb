require "test_helper"
class UserControllerTest < ActionController::TestCase

  context "create" do
    context "valid User" do

      setup do
        @user = Factory.build(:valid_user)
      end

      should "return success when trying to create a user" do
        post :create, :user => @user
        assert_response 200
      end

      should "create user" do
        assert_difference('User.count') do
          post :create, :user => @user
        end
      end

    end
  end

  context "delete" do
      context "valid user" do

        setup do
          @user = Factory(:valid_user)
        end

        should "return success when trying to delete a user" do
          delete :destroy,  :id => @user.id
          assert_response 200
        end

        should "delete user" do
          assert_difference('User.count', -1) do
            delete :destroy, :id => @user.id
          end
        end
      end

      context "invalid user" do
        should "return missing when trying to delete a user" do
          delete :destroy,  :id => "invalid_user_id"
          assert_response 404
        end

        should "not delete user" do
          assert_difference('User.count', 0) do
            delete :destroy, :id => "invalid_user_id"
          end
        end

      end
    end

  context "show" do
    context "valid user" do
      setup do
        @user = Factory(:valid_user)
      end

      should "return success when trying to show a user" do
        get :show, :id => @user.id
        assert_response 200
      end

      should "show a user" do
        get :show, :id => @user.id
        assert @user, @response.body
      end
    end

    context "invalid user" do
      should "return missing when trying to show a user" do
        get :show, :id => "invalid_user_id"
        assert_response 404
      end
    end
  end

  context "index" do
    setup do
      @users = []
      @users << Factory(:valid_user)
    end

    should "return all userss" do
      get :index
      assert @users, @response.body
    end

    should "return success when trying to list all users" do
      get :index
      assert_response 200
    end
  end

end

