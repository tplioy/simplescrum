require 'test_helper'

class ProjectsControllerTest < ActionController::TestCase

  context "valid routes" do
    should "create a project through this route" do
      assert_recognizes({ :controller => 'projects', :action => 'create'},
                        { :path => 'projects/', :method => :post })
    end

    should "show all projects through this route" do
      assert_recognizes({ :controller => 'projects', :action => 'index' }, { :path => 'projects/', :method => :get })
    end

    should "delete a project through this route" do
      assert_recognizes({ :controller => 'projects', :action => 'destroy' , :id => '1' }, { :path => 'projects/1', :method => :delete })
    end

    should "show a project through this route" do
      assert_recognizes({ :controller => 'projects', :action => 'show' , :id => '1' }, { :path => 'projects/1', :method => :get })
    end
  end


  context "create" do
    context "valid Project" do
      setup do
        @project = Factory.build(:valid_project)
      end

      should "return success when trying to create a project" do
        post :create, :project => @project
        assert_response 200
      end

      should "create project" do
        assert_difference('Project.count') do
          post :create, :project => @project
        end
      end
    end
  end

  context "delete" do
    context "valid Project" do
      setup do
        @project = Factory(:valid_project)
      end

      should "return success when trying to delete a project" do
        delete :destroy, :id => @project.id
        assert_response 200
      end

      should "delete project" do
        assert_difference('Project.count', -1) do
          delete :destroy, :id => @project.id
        end
      end
    end

    context "invalid Project" do
      should "return success when trying to delete a project" do
        delete :destroy, :id => "invalid_project_id"
        assert_response 404
      end
      should "not delete project" do
        assert_difference('Project.count', 0) do
          delete :destroy, :id => "invalid_project_id"
        end
      end
    end
  end

  context "index" do
    setup do
      @projects = []
      @projects << Factory(:valid_project)
    end

    should "return all projects" do
      get :index
      assert @projects, @response.body
    end

    should "return success when trying to list all projects" do
      get :index
      assert_response 200
    end

  end

  context "show" do
    context "valid project" do
      setup do
        @project = Factory(:valid_project)
      end

      should "return success when trying to show a project" do
        get :show, :id => @project.id
        assert_response 200
      end

      should "show a project" do
        get :show, :id => @project.id
        assert @project, @response.body
      end
    end

    context "invalid project" do
      should "return missing when trying to show a project" do
        get :show, :id => "invalid_project_id"
        assert_response 404
      end
    end
  end

end
