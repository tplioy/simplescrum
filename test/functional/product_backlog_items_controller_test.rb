require 'test_helper'

class ProductBacklogItemsControllerTest < ActionController::TestCase
  context "valid routes" do
    should "create a productBacklogItem for a project through this route" do
      assert_recognizes({ :controller => 'product_backlog_items', :action => 'create', :id => '1'},
                        { :path => 'projects/1/backlog/items', :method => :post })
    end

    should "delete a productBacklogItem of a project through this route" do
      assert_recognizes({ :controller => 'product_backlog_items', :action => 'destroy', :project_id => '1', :id => '1' },
                        { :path => 'projects/1/backlog/items/1', :method => :delete })
    end

    should "show a productBacklogItem of a project through this route" do
      assert_recognizes({ :controller => 'product_backlog_items', :action => 'show', :project_id => '1', :id => '1' },
                        { :path => 'projects/1/backlog/items/1', :method => :get })
    end
    should "update a productBacklogItem of a project through this route" do
      assert_recognizes({ :controller => 'product_backlog_items', :action => 'update', :project_id => '1', :id => '1' },
                        { :path => 'projects/1/backlog/items/1', :method => :put })
    end
  end

  context "index" do
    context "valid Project" do
      setup do
        @backlog = []
        @backlog << Factory(:valid_backlog_item)
      end

      should "return all projects" do
        get :index , :id => @backlog.first.project.id
        assert @backlog, @response.body
      end

      should "return success when trying to list all projects" do
        get :index , :id => @backlog.first.project.id
        assert_response 200
      end
    end
    context "invalid Project" do
      should "not return all projects" do
        get :index , :id => "invalid_project_id"
        assert "not found", @response.body
      end

      should "return success when trying to list all projects" do
        get :index , :id => "invalid_project_id"
        assert_response 404
      end

    end
  end



  context "delete" do
    context "valid ProductBacklogItem" do
      context "valid Project" do
        setup do
          @backlog_item = Factory(:valid_backlog_item)
        end

        should "return success when trying to delete a productBacklogItem" do
          delete :destroy, :project_id => @backlog_item.project_id, :id => @backlog_item.id
          assert_response 200
        end

        should "delete productBacklogItem" do
          assert_difference('ProductBacklogItem.count', -1) do
            delete :destroy, :project_id => @backlog_item.project_id, :id => @backlog_item.id
          end
        end
      end

      context "invalid Project" do
        setup do
          @backlog_item = Factory(:valid_backlog_item)
        end
        should "return missing when trying to delete a productBacklogItem" do
          delete :destroy, :project_id => "invalid_project_id", :id => @backlog_item.id
          assert_response 404
        end
        should "not delete productBacklogItem" do
          assert_difference('Project.count', 0) do
            delete :destroy, :project_id => "invalid_project_id", :id => @backlog_item.id
          end
        end
      end
    end
    context "invalid ProductBacklogItem" do
      context "valid Project" do
        setup do
          @project = Factory(:valid_project)
        end

        should "return missing when trying to delete a productBacklogItem" do
          delete :destroy, :project_id => @project.id, :id => "invalid_backlog_item_id"
          assert_response 404
        end

        should "not delete productBacklogItem" do
          assert_difference('ProductBacklogItem.count', 0) do
            delete :destroy, :project_id => @project.id, :id => "invalid_backlog_item_id"
          end
        end
      end

      context "invalid Project" do
        should "return success when trying to delete a productBacklogItem" do
          delete :destroy, :project_id => "invalid_project_id", :id => "invalid_backlog_item_id"
          assert_response 404
        end
        should "not delete productBacklogItem" do
          assert_difference('Project.count', 0) do
            delete :destroy, :project_id => "invalid_project_id", :id => "invalid_backlog_item_id"
          end
        end
      end

    end


    context "create" do
      context "valid ProductBacklogItem" do

        context "valid project" do
          setup do
            @project = Factory(:valid_project)
            @backlog_item = Factory.build(:valid_backlog_item)
          end

          should "return success when trying to create a productBacklogItem" do
            post :create,  :id => @project.id, :item => @backlog_item
            assert_response 200
          end

          should "create productBacklogItem" do
            assert_difference('ProductBacklogItem.count') do
              post :create,  :id => @project.id, :item => @backlog_item
            end
          end
        end

        context "invalid project" do
          setup do
            @backlog_item = Factory.build(:valid_backlog_item)
          end

          should "return missing when trying to create a productBacklogItem" do
            post :create,  :id => "invalid_project_id", :item => @backlog_item
            assert_response 404
          end

          should "not create productBacklogItem" do
            assert_difference('ProductBacklogItem.count', 0) do
              post :create,  :id => "invalid_project_id", :item => @backlog_item
            end
          end
        end

      end


    end
  end

end
