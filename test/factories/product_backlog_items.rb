# == Schema Information
#
# Table name: product_backlog_items
#
#  id                 :integer         not null, primary key
#  product_backlog_id :integer
#  name               :string(255)
#  estimate           :integer
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#

# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :valid_backlog_item, :class => ProductBacklogItem do
    project {Factory(:valid_project)}
    name "Valid Item"
    estimate 2
  end
end
