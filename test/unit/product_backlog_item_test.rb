# == Schema Information
#
# Table name: product_backlog_items
#
#  id                 :integer         not null, primary key
#  product_backlog_id :integer
#  name               :string(255)
#  estimate           :integer
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#

require 'test_helper'

class ProductBacklogItemTest < ActiveSupport::TestCase
  should belong_to(:project)
  should validate_presence_of(:project_id)
end
