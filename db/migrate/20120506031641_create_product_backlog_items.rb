class CreateProductBacklogItems < ActiveRecord::Migration
  def up
    create_table :product_backlog_items do |t|
      t.references :project
      t.string :name
      t.integer :estimate
      t.timestamps
    end
  end

  def down
    remove_column :product_backlog_items, :project_id
  end
end
