SimpleScrum::Application.routes.draw do
  resources :projects, :except=> [:new, :edit]




  get "/projects/:id/backlog/items/" => "product_backlog_items#index"
  get "/projects/:project_id/backlog/items/:id" => "product_backlog_items#show"
  post "/projects/:id/backlog/items/" => "product_backlog_items#create"
  put "/projects/:project_id/backlog/items/:id" => "product_backlog_items#update"
  delete "/projects/:project_id/backlog/items/:id" => "product_backlog_items#destroy"

  resources :users, :except => [:new, :edit]
end
